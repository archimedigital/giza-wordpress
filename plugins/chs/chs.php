<?php

/**
 * @package WordPress
 * @subpackage CHS Wordpress
 *
 * Plugin Name: IIP
 * Description: Plugin associated with the IIP Wordpress website
 * Version: 1.0.0
 * Author: Archimedes Digital
 */

define('CHS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

// acf fields
// require_once( CHS_PLUGIN_DIR .'acf/annotations.php.inc');

// require settings
require_once( CHS_PLUGIN_DIR .'config/settings.php.inc');
require_once( CHS_PLUGIN_DIR .'config/post_types.php.inc');
require_once( CHS_PLUGIN_DIR .'config/taxonomies.php.inc');

// require other specific plugin functionalities
require_once( CHS_PLUGIN_DIR .'inc/ajax.php.inc');
require_once( CHS_PLUGIN_DIR .'inc/annotations.php.inc');
require_once( CHS_PLUGIN_DIR .'inc/bookshelf.php.inc');
require_once( CHS_PLUGIN_DIR .'inc/dashboard.php.inc');
require_once( CHS_PLUGIN_DIR .'inc/meta_keyword_search.php.inc');
require_once( CHS_PLUGIN_DIR .'inc/options-page.php.inc');
require_once( CHS_PLUGIN_DIR .'inc/shortcodes.php.inc');
require_once( CHS_PLUGIN_DIR .'inc/post_counts.php.inc');

// require epub library
require_once( CHS_PLUGIN_DIR .'php-epub-0.1.2/src/EpubUtil.php');
require_once( CHS_PLUGIN_DIR .'php-epub-0.1.2/src/EpubParser.php');


add_filter('xmlrpc_enabled', '__return_false');
