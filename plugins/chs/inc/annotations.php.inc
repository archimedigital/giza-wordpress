<?php

// Register a REST API endpoint..
add_action( 'rest_api_init', 'chs_register_annotation_rest_routes' );
function chs_register_annotation_rest_routes() {
  register_rest_route( 'chs/v1', '/annotation/(?<id>.*)', array(
    array(
      'methods'  => 'DELETE',
      'callback' => function ( $request ) {
        error_log(var_export($request->get_params()['id'], true));
        wp_delete_post($request->get_params()['id']);
        return array("response" => "Annotation deleted");
      },
    ),
    array(
      'methods'  => 'POST',
      'callback' => function ( $request ) {
        error_log(var_export($request->get_params(), true));
        wp_update_post(array(
          "ID" => $request->get_params()['id'],
          "post_content" => $request->get_params()['content'],
        ));
        return array("response" => "Annotation updated");
      },
    ),
  ));

  register_rest_route( 'chs/v1', '/annotation/', array(
    array(
      'methods'  => 'PUT',
      'callback' => function ( $request ) {
        error_log(var_export($request->get_params(), true));

        $on_post = get_post($request->get_params()['on_post']);
        $post_id = wp_insert_post(array(
          'post_author' => $request->get_params()['post_author'],
          'post_title' => 'Annotation on ' . $on_post->post_title,
          'post_status' => 'publish',
          'post_type' => 'annotation',
          'post_content' => $request->get_params()['content'],
        ));

        add_post_meta($post_id, 'on_post', $request->get_params()['on_post']);
        add_post_meta($post_id, 'paragraph_n', $request->get_params()['paragraph_n']);
        if ($request->get_params()['parent_comment_id']) {
          add_post_meta($post_id, 'parent_comment_id', $request->get_params()['parent_comment_id']);
        }

        return array("response" => "Annotation created");
      },
    ),
  ));
}
