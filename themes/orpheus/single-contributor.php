<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */
global $params;
$context         = Timber::context();
$post     = Timber::query_post($params['id']);
$context['post'] = $post;
$context['page_title'] = $post->title;
$context['attachments'] = array();
$query = array('post_type' => 'post', 'posts_per_page' => 12);
$posts = Timber::get_posts($query);
$context['posts'] = new Timber\PostQuery($query);

$post->meta = get_post_meta($post->ID);
if ($post->meta['attachments']) {
	$post->meta['attachments'] = JSON_decode($post->meta['attachments'][0]);
	$post->meta['attachments'] = $post->meta['attachments']->attachments;

	foreach($post->meta['attachments'] as $attachment) {
		$attachment->url = "/" . $post->category . "/full/" . $attachment->fields->title;

		$file_is_image = false;
		$image_filename_endings = array("jpeg", "jpg", "png", "tiff", "tif");
		foreach ($image_filename_endings as $image_filename_ending) {
			$length = strlen($image_filename_ending);
			if (substr($attachment->fields->title, -$length) === $image_filename_ending){
				$file_is_image = true;
			}
		}
		if ($file_is_image) {
			$context['attachments'][] = $attachment;
		}
	}
}
$context['post']->meta = get_post_meta($post->ID);
$context['tags'] = Timber::get_terms(array( 'taxonomy' => 'post_tag', 'hide_empty' => false, 'number' => 12, 'orderby' => 'count' ));

// track post view
wpb_set_post_views($post->ID);

Timber::render( array( 'single-contributor.twig' ), $context );
