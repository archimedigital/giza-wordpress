<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * Methods for TimberHelper can be found in the /functions sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::context();

$expressionsOfWoe = array(
    'ἰὼ ἰώ',
    'αἶ, αἶ',
    'οἴμοι μοι',
    'φεῦ φεῦ',
    'ἰώ μοί μοι',
    'ὦ Ζεῦ',
    'βοᾷ βοᾷ',
    'αἰαῖ αἰαῖ',
    'ἔα ἔα',
    'ὀττοτοτοτοτοῖ',
    'ἄλγος ἄλγος βοᾷς',
    'ἐλελεῦ',
    'μὴ γένοιτο',
    'οὐαί',
  );
$context['expression_of_woe'] = $expressionsOfWoe[array_rand($expressionsOfWoe)];


Timber::render( '404.twig', $context );
