<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'page-primary-source.twig' );

$context = Timber::context();


$primary_sources = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "primary-source", 'order' => "ASC", 'orderby' => "title" ));
$context['primary_sources_a_f'] = array();
$context['primary_sources_g_l'] = array();
$context['primary_sources_m_r'] = array();
$context['primary_sources_s_z'] = array();

function lstrip($str, $prefix) {
	if (substr($str, 0, strlen($prefix)) == $prefix) {
	    $str = substr($str, strlen($prefix));
	}

	return $str;
}


foreach ($primary_sources as $primary_source) {
	$primary_source_title_clean = strtolower($primary_source->post_title);
	$primary_source_title_clean = lstrip($primary_source_title_clean, "the ");
	$primary_source_title_clean = lstrip($primary_source_title_clean, "a ");
	$primary_source_title_clean = ltrim($primary_source_title_clean);
	$primary_source_title_clean = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $primary_source_title_clean);
	$first_char_ord = ord($primary_source_title_clean[0]);
	if (ord("a") <= $first_char_ord && $first_char_ord <= ord("f") ) {
		$context['primary_sources_a_f'][] = $primary_source;
	} else if (ord("g") <= $first_char_ord && $first_char_ord <= ord("l") ) {
		$context['primary_sources_g_l'][] = $primary_source;
	} else if (ord("m") <= $first_char_ord && $first_char_ord <= ord("r") ) {
		$context['primary_sources_m_r'][] = $primary_source;
	} else if (ord("s") <= $first_char_ord && $first_char_ord <= ord("z") ) {
		$context['primary_sources_s_z'][] = $primary_source;
	}

	$primary_source->_title_clean = $primary_source_title_clean;
}

usort($context['primary_sources_a_f'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});

usort($context['primary_sources_g_l'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});

usort($context['primary_sources_m_r'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});

usort($context['primary_sources_s_z'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});

Timber::render( $templates, $context );
