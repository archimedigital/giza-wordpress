const bookOfSecrets = {
    "work": {
      "id": 1,
      "slug": "book-of-secrets",
      "english_title": "Book of Secrets",
      "original_title": "Le livre des secrets",
      "form": null,
      "work_type": "edition",
      "label": "Iliad, Homeri Opera",
      "description": "Newton Transcription",
      "urn": "urn:cts:dhLab:flamel.bookOfSecrets",
      "full_urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton",
      "language": {
        "id": 2,
        "title": "English",
        "slug": "english"
      },
      "version": {
        "id": 1,
        "slug": "newton-transcription",
        "title": "Book of Secrets, trascribed by Issac Newton",
        "description": null
      },
      "exemplar": null,
      "refsDecls": [
        {
          "description": "This pointer pattern extracts Paragraph",
          "label": "paragraph"
        }
      ],
      "translation": null,
      "textNodes": [
        {
          "id": 1,
          "index": 0,
          "location": [
            1
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton:1",
          "text": "The book"
        },

        {
          "id": 1,
          "index": 2,
          "location": [
            3
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton:3",
          "text": "of"
        },

        {
          "id": 1,
          "index": 4,
          "location": [
            5
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton:5",
          "text": "Nicholas Flamel"
        },

        {
          "id": 1,
          "index": 6,
          "location": [
            7
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton:7",
          "text": "conteining"
        },

        {
          "id": 1,
          "index": 8,
          "location": [
            9
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton:9",
          "text": "The explication of the Hieroglyphical Figures which he caused to be put on the Church of the SS. Innocents at Paris."
        },
        {
          "id": 1,
          "index": 9,
          "location": [
            10
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton:10",
          "text": "The Preface"
        },
        {
          "id": 1,
          "index": 10,
          "location": [
            11
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton:11",
          "text": "Let the Lord my God be eternally praised who exalts the humble aloft and makes the heart of those who trust in him to rejoice: who opens freely the fountains of his bounty to them that believe and puts under their feet the worldly orbs of all earthly felicities. In him be always our hope, in his fear our felicity, in his mercy the glory of the reparation of our nature and in our prayer a surety immoveable. And thou o almighty God, as thy bounty hath deigned to work in the earth before me thy unworthy servant, all the treasures of the riches of the world, may it please thy clemency, when I shall be no longer of the number of the living, to work for me again the treasures of the heavens & let me behold thy divine face whereof the majesty is an ineffable delight & whereof the ravishment is never shewn in the heart of a living man. This I request by the Lord Jesus Christ thy well beloved son who"
        },
        {
          "id": 1,
          "index": 11,
          "location": [
            12
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton:12",
          "text": "Although that I, Nicholas Flamel, Scrivener & inhabitant of Paris in this year one thousand three hundred ninety nine & dwelling in my house in the street of the Scriveners neare the chappel of St James of the Butchers; although, say I, that I had got but a little latin, according to the means of my Parents, who yet were even by those that envied me esteemed men of probity: yet it is true that (by the great grace of God & the in-tercession of the blessed saints & sainets of Paradise, principally of Saint James) I did understand fully the Philosophers books & apprehend their secrets so much hidden. For this reason there shall be no moment of my life but that rembring that great benefit & kneeling (if the place permit) or in my heart with all my affection I will render thanks to the most benign God who suffers not the child of the just to begg from door to door & who deceives not them who hope entirely in his benediction. Therefore whilst after the decease of my Parents I got my living in our art of writing, making Inventories, casting accounts & stating the expenses of Tutors and Pupills, there fell into my hands for the summ of two florens a Book guilded very old & pretty large. It was not of paper or parchment as others are, but it was made of barks (as it seemed to me) of tender trees bound together. Its cover was of copper well bound, all engraven with strange letters or figures, & as for me, I believe they might well be Greek characters or of some other ancient language. This much I can say that I could not read them & that I know well that they were neither French nor latin marks or letters, for I understand them a little. As to the inside, the leaves of bark were graven & with very great industry written with a point of iron in fair & very neat latin letters coloured. It contained three times seven leaves. For they were so numbered on the top of the leaves, the seventh of which was always without writing. In the place of which there was painted in the first seventh leafe a Rod & [two] serpents eating one another. In the second seventh a Cross where a Serpent was crucified. In the last seventh were painted Deserts or wildernesses in the midst whereof ran many fair fountains from whence there issued out many serpents which ran up & down."
        },
        {
          "id": 1,
          "index": 12,
          "location": [
            13
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton:13",
          "text": "In the first of the leaves there was written in great capital letters guilded, Abraham the Jew, Prince, Priest, Levite, Astrologer & Philosopher of the nation of the Jews by the wrath of God dispersed among the Gaules Health D. I. After this it was filled with great curses and maledictions (with the word Maranatha which was often there repeated) against every person who should cast his eyes upon it, if he were not a sacrificer or scribe. He that sold me this book knew not the value of it, no more than I did when I bought it. I believe it had been taken from the miserable Jews or found somewhere hidden in an ancient place of their aboad."
        },

        {
          "id": 1,
          "index": 13,
          "location": [
            14
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton:14",
          "text": "In this book on the second leaf he comforted his nation exhorting them to shun vices & chiefly Idolatry, expecting the coming of the Messias with sweet patience, who should conquer all the kings of the earth & reign with his people in glory eternally. Without doubt, this was a very knowing man."
        },

        {
          "id": 1,
          "index": 14,
          "location": [
            15
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton:15",
          "text": "On the third leafe & in all the others which followed & were written, to help his captivated nation to pay the tributes to the Roman Emperors & to do other things which I will not tell, He taught them the transmutation of mettalls in vulgar words, painted the vessels by the side or margin & advertised them of the colours & of all the rest except the first agent whereof he spake not a word, but painted it well, as he said and figured it with very great artifice on the fourth & fifth leaves entire. For although it was very intelligibly figured & painted, yet one could not understand it without being far advanced in their traditionall cabala & without having studied well the books of the Philosophers. Therefore the fourth & fifth leaf was without writing, being wholy filled with fair figures enlightened or painted with great artifice."
        }
      ]
    }
};

const bookOfSecretsPcss = {
    "work": {
      "id": 1,
      "slug": "book-of-secrets",
      "english_title": "Book of Secrets",
      "original_title": "Le livre des secrets",
      "form": null,
      "work_type": "edition",
      "label": "Iliad, Homeri Opera",
      "description": "Newton Transcription",
      "urn": "urn:cts:dhLab:flamel.bookOfSecrets",
      "full_urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss",
      "language": {
        "id": 2,
        "title": "English",
        "slug": "english"
      },
      "version": {
        "id": 1,
        "slug": "newton-transcription",
        "title": "Book of Secrets, trascribed by Issac Newton",
        "description": null
      },
      "exemplar": null,
      "refsDecls": [
        {
          "description": "This pointer pattern extracts Paragraph",
          "label": "paragraph"
        }
      ],
      "translation": null,
      "textNodes": [
        {
          "id": 1,
          "index": 0,
          "location": [
            1
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:1",
          "text": "infant who is endowed (by the living God) with a "
        },

        {
          "id": 1,
          "index": 2,
          "location": [
            3
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:3",
          "text": "vegetable soul. Which thing is a secret most ad"
        },

        {
          "id": 1,
          "index": 4,
          "location": [
            5
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:5",
          "text": "mirable &amp; most hidden, which hath befooled for want "
        },

        {
          "id": 1,
          "index": 6,
          "location": [
            7
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:7",
          "text": "of understanding it, all those who have <br/>searched without finding it <br/>&amp; which hath made wise every man who hath beheld"
        },

        {
          "id": 1,
          "index": 8,
          "location": [
            9
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:9",
          "text": " it with the eyes of his body or of his mind."
        },
        {
          "id": 1,
          "index": 9,
          "location": [
            10
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:10",
          "text": "You must therefore divide the coagulated "
        },
        {
          "id": 1,
          "index": 10,
          "location": [
            11
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:11",
          "text": "body into two parts or portions, one of which called Azoth"
        },
        {
          "id": 1,
          "index": 11,
          "location": [
            12
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:12",
          "text": "must be kept for washing &amp; mundifying the other"
        },
        {
          "id": 1,
          "index": 12,
          "location": [
            13
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:13",
          "text": "called Leton which must be whitened."
        },

        {
          "id": 1,
          "index": 13,
          "location": [
            14
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:14",
          "text": "That which is washed, is the serpent Python who"
        },

        {
          "id": 1,
          "index": 14,
          "location": [
            15
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:15",
          "text": "slime of the earth assembled by the waters of "
        },
        {
          "id": 1,
          "index": 15,
          "location": [
            16
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:15",
          "text": "the deluge when all the confections were water"
        },
        {
          "id": 1,
          "index": 16,
          "location": [
            17
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:15",
          "text": "ought to be slain &amp; vanquished by the arrows "
        },
        {
          "id": 1,
          "index": 17,
          "location": [
            18
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:15",
          "text": "of the God Apollo, the fair Sun, that is by our fire "
        },
        {
          "id": 1,
          "index": 18,
          "location": [
            19
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:15",
          "text": "equal to that of the Sun."
        },
        {
          "id": 1,
          "index": 19,
          "location": [
            20
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:15",
          "text": "Hee who washes or rather the washings which"
        },
        {
          "id": 1,
          "index": 20,
          "location": [
            21
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:15",
          "text": "must be continued with the other moity, are the <br/>teeth of the serpent which the sage Operator, the"
        },
        {
          "id": 1,
          "index": 21,
          "location": [
            22
          ],
          "urn": "urn:cts:dhLab:flamel.bookOfSecrets.transcriptionNewton.pcss:15",
          "text": "valiant <br>Theseus <br>, will sow in the same earth out of"
        },
      ]
    }
}
export default bookOfSecrets;
export { bookOfSecretsPcss };
