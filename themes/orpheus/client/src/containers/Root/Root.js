import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { CookiesProvider } from 'react-cookie';
import { SnackbarProvider } from 'notistack';
import { ThemeProvider } from '@material-ui/core/styles';

import CustomTheme from '../../lib/muiTheme.js';
import store from '../../store';

const Root = (props) => (
	<ReduxProvider store={store}>
		<CookiesProvider>
			<ThemeProvider theme={CustomTheme}>
				<SnackbarProvider anchorOrigin={{
	        vertical: 'bottom',
	        horizontal: 'right',
    		}}>
					{props.children}
				</SnackbarProvider>
			</ThemeProvider>
		</CookiesProvider>
	</ReduxProvider>
);

export default Root;
