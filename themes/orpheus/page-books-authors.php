<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array( 'page-books-terms.twig' );
$context = Timber::context();
$authors = get_terms( 'author', array(
    'hide_empty' => false,
) );

$context['terms_a_f'] = array();
$context['terms_g_l'] = array();
$context['terms_m_r'] = array();
$context['terms_s_z'] = array();



foreach ($authors as $author) {
	$author_last_name_clean = strtolower(end(explode(" ", $author->name)));
	$author_last_name_clean = ltrim($author_last_name_clean);
	$author_last_name_clean = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $author_last_name_clean);
	$first_char_ord = ord($author_last_name_clean[0]);
	if (ord("a") <= $first_char_ord && $first_char_ord <= ord("f") ) {
		$context['terms_a_f'][] = $author;
	} else if (ord("g") <= $first_char_ord && $first_char_ord <= ord("l") ) {
		$context['terms_g_l'][] = $author;
	} else if (ord("m") <= $first_char_ord && $first_char_ord <= ord("r") ) {
		$context['terms_m_r'][] = $author;
	} else if (ord("s") <= $first_char_ord && $first_char_ord <= ord("z") ) {
		$context['terms_s_z'][] = $author;
	}

	$author->_title_clean = $author_last_name_clean;
}

usort($context['terms_a_f'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});
usort($context['terms_g_l'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});
usort($context['terms_m_r'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});
usort($context['terms_s_z'], function($a, $b) {
    return strcmp($a->_title_clean, $b->_title_clean);
});


Timber::render( $templates, $context );
